package ch.swissbytes.module;

import ch.swissbytes.ExamplesecurityConfiguration;
import ru.vyarus.dropwizard.guice.module.support.DropwizardAwareModule;

/**
 * Created by dbati on 17/8/2017.
 */
public class MainModule extends DropwizardAwareModule<ExamplesecurityConfiguration> {
  @Override
  protected void configure() {

  }
}
