package ch.swissbytes.resources.example;

import ch.swissbytes.api.Credential;
import ch.swissbytes.api.CredentialRole;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by dbati on 17/8/2017.
 */
@Path("example")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ExampleResource {

  @GET
  @Path("hello")
  public Response hello() {
    Credential credential = new Credential();
    credential.id = 1;
    credential.username = "example";
    credential.password = "1234";
    credential.role = CredentialRole.ADMIN;
    return Response.ok(credential).build();
  }

  @GET
  @Path("hello_security")
  public Response helloSecurity() {
    return Response.ok("hola").build();
  }
}
