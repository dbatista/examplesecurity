package ch.swissbytes.api;

/**
 * Created by dbati on 17/8/2017.
 */
public class Credential {
  public long id;
  public String username;
  public String password;
  public CredentialRole role;
}
