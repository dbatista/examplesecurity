package ch.swissbytes.api;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by dbati on 17/8/2017.
 */
public enum CredentialRole {
  SUPERADMIN(0), ADMIN(1), CUSTOMER(2);

  private static final Map<Integer, CredentialRole> lookup = new HashMap<>();

  static {
    for (CredentialRole s : EnumSet.allOf(CredentialRole.class)) {
      lookup.put(s.getValue(), s);
    }
  }

  private int code;

  CredentialRole(int code) {
    this.code = code;
  }

  public static CredentialRole get(int code) {
    return lookup.get(code);
  }

  public int getValue() {
    return code;
  }
}
