package ch.swissbytes;

import ch.swissbytes.module.MainModule;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import ru.vyarus.dropwizard.guice.GuiceBundle;

public class ExamplesecurityApplication extends Application<ExamplesecurityConfiguration> {

    public static void main(final String[] args) throws Exception {
        new ExamplesecurityApplication().run(args);
    }

    @Override
    public String getName() {
        return "examplesecurity";
    }

    @Override
    public void initialize(final Bootstrap<ExamplesecurityConfiguration> bootstrap) {
        // TODO: application initialization
        bootstrap.addBundle(GuiceBundle.builder()
            .enableAutoConfig(this.getClass().getPackage().getName())
            .modules(new MainModule())
            .build());
    }

    @Override
    public void run(final ExamplesecurityConfiguration configuration,
                    final Environment environment) {
        // TODO: implement application
    }

}
